FROM python:3.8-slim

WORKDIR /app
COPY --chown=1001:0 ./requirements.txt /app/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

ENV MLFLOW_MODEL_PATH="models:/karpov_model/production"
COPY ./src /app
CMD ["python", "app.py"]
