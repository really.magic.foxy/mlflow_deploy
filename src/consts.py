import os
from pathlib import Path

from dotenv import dotenv_values

PROJECT_PATH = Path(__file__).parent.parent

ENV_PATH = PROJECT_PATH / '.env'
config_env = dotenv_values(ENV_PATH)

THRESHOLD_0_1 = float(os.environ.get(
    "THRESHOLD_0_1", config_env.get("THRESHOLD_0_1")))
THRESHOLD_1_2 = float(os.environ.get(
    "THRESHOLD_1_2", config_env.get("THRESHOLD_1_2")))
MLFLOW_MODEL_PATH = os.environ.get(
    "MLFLOW_MODEL_PATH", config_env.get("MLFLOW_MODEL_PATH"))
