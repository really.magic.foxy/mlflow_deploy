import mlflow
import numpy as np
from typing import Tuple

from consts import THRESHOLD_0_1, THRESHOLD_1_2


class SklearnModelWrapper(mlflow.pyfunc.PythonModel):
    def __init__(self, model):
        self.model = model

    def predict(self, context, model_input):
        pred = self.model.predict(model_input)[0]
        class_str = 'virginica'
        if pred < THRESHOLD_0_1:
            class_str = 'setosa'
        elif pred > THRESHOLD_0_1 and pred < THRESHOLD_0_1:
            class_str = 'versicolor'
        answer = {"class": pred,
                  "class_str": class_str,
                  "threshold_0_1": THRESHOLD_0_1,
                  "threshold_1_2": THRESHOLD_1_2}
        return answer
