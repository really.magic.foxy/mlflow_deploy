from flask import Flask, request, jsonify
from consts import MLFLOW_MODEL_PATH
import mlflow
import os
import numpy as np


def set_credentials():
    os.environ["AWS_ACCESS_KEY_ID"] = "OLGA_ACCESS_KEY"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "OLGA_SECRET_KEY"
    # s3 protocol
    os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://65.21.147.64:9000"
    mlflow.set_tracking_uri("http://65.21.147.64:5000")


set_credentials()
loaded_model = mlflow.pyfunc.load_model(MLFLOW_MODEL_PATH)
app = Flask(__name__)


@app.route('/', methods=['GET'])
def index():
    return "Server is running."


@app.route('/get_source_iris_pred', methods=['GET'])
def get_source_iris_pred():
    sepal_length = float(request.args.get('sepal_length', 0))
    sepal_width = float(request.args.get('sepal_width', 0))
    model_input = np.array([[sepal_length, sepal_width]])

    prediction = loaded_model.predict(model_input)['class']

    return jsonify({"prediction": prediction})


@app.route('/get_string_iris_pred', methods=['GET'])
def get_string_iris_pred():
    sepal_length = float(request.args.get('sepal_length', 0))
    sepal_width = float(request.args.get('sepal_widt', 0))
    return "501", 501


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5003)
